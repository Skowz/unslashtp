package bertrand.maxence.unsplashTP.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\bf\u0018\u00002\u00020\u0001J2\u0010\u0002\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u00072\b\b\u0001\u0010\b\u001a\u00020\u00072\b\b\u0001\u0010\t\u001a\u00020\nH\'\u00a8\u0006\u000b"}, d2 = {"Lbertrand/maxence/unsplashTP/services/ApiInterface;", "", "getPhotos", "Lretrofit2/Call;", "", "Lbertrand/maxence/unsplashTP/models/Photo;", "page", "", "pageLimit", "order", "", "app_debug"})
public abstract interface ApiInterface {
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "photos")
    public abstract retrofit2.Call<java.util.List<bertrand.maxence.unsplashTP.models.Photo>> getPhotos(@retrofit2.http.Query(value = "page")
    int page, @retrofit2.http.Query(value = "per_page")
    int pageLimit, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "order_by")
    java.lang.String order);
}