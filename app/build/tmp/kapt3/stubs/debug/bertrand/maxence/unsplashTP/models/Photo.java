package bertrand.maxence.unsplashTP.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u001c\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001BU\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\u0003\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0011J\t\u0010!\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\"\u001a\u00020\u0010H\u00c6\u0003J\t\u0010#\u001a\u00020\u0003H\u00c6\u0003J\t\u0010$\u001a\u00020\u0003H\u00c6\u0003J\t\u0010%\u001a\u00020\u0003H\u00c6\u0003J\t\u0010&\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\'\u001a\u00020\tH\u00c6\u0003J\t\u0010(\u001a\u00020\u000bH\u00c6\u0003J\t\u0010)\u001a\u00020\u0003H\u00c6\u0003J\t\u0010*\u001a\u00020\u000eH\u00c6\u0003Jm\u0010+\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\u000b2\b\b\u0002\u0010\f\u001a\u00020\u00032\b\b\u0002\u0010\r\u001a\u00020\u000e2\b\b\u0002\u0010\u000f\u001a\u00020\u0010H\u00c6\u0001J\u0013\u0010,\u001a\u00020-2\b\u0010.\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010/\u001a\u000200H\u00d6\u0001J\t\u00101\u001a\u00020\u0003H\u00d6\u0001R\u0016\u0010\u0006\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0007\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0013R\u0016\u0010\u000f\u001a\u00020\u00108\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0005\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0013R\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0013R\u0016\u0010\f\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0013R\u0016\u0010\r\u001a\u00020\u000e8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0016\u0010\b\u001a\u00020\t8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u0016\u0010\n\u001a\u00020\u000b8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u0016\u0010\u0004\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b \u0010\u0013\u00a8\u00062"}, d2 = {"Lbertrand/maxence/unsplashTP/models/Photo;", "", "id", "", "width", "height", "color", "description", "url", "Lbertrand/maxence/unsplashTP/models/PhotoUrl;", "user", "Lbertrand/maxence/unsplashTP/models/User;", "likes", "sponsorship", "Lbertrand/maxence/unsplashTP/models/Sponsorship;", "exif", "Lbertrand/maxence/unsplashTP/models/Exif;", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lbertrand/maxence/unsplashTP/models/PhotoUrl;Lbertrand/maxence/unsplashTP/models/User;Ljava/lang/String;Lbertrand/maxence/unsplashTP/models/Sponsorship;Lbertrand/maxence/unsplashTP/models/Exif;)V", "getColor", "()Ljava/lang/String;", "getDescription", "getExif", "()Lbertrand/maxence/unsplashTP/models/Exif;", "getHeight", "getId", "getLikes", "getSponsorship", "()Lbertrand/maxence/unsplashTP/models/Sponsorship;", "getUrl", "()Lbertrand/maxence/unsplashTP/models/PhotoUrl;", "getUser", "()Lbertrand/maxence/unsplashTP/models/User;", "getWidth", "component1", "component10", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "other", "hashCode", "", "toString", "app_debug"})
@kotlinx.serialization.Serializable()
public final class Photo {
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "id")
    private final java.lang.String id = null;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "width")
    private final java.lang.String width = null;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "height")
    private final java.lang.String height = null;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "color")
    private final java.lang.String color = null;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "alt_description")
    private final java.lang.String description = null;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "urls")
    private final bertrand.maxence.unsplashTP.models.PhotoUrl url = null;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "user")
    private final bertrand.maxence.unsplashTP.models.User user = null;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "likes")
    private final java.lang.String likes = null;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "sponsorship")
    private final bertrand.maxence.unsplashTP.models.Sponsorship sponsorship = null;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "exif")
    private final bertrand.maxence.unsplashTP.models.Exif exif = null;
    
    @org.jetbrains.annotations.NotNull()
    public final bertrand.maxence.unsplashTP.models.Photo copy(@org.jetbrains.annotations.NotNull()
    java.lang.String id, @org.jetbrains.annotations.NotNull()
    java.lang.String width, @org.jetbrains.annotations.NotNull()
    java.lang.String height, @org.jetbrains.annotations.NotNull()
    java.lang.String color, @org.jetbrains.annotations.NotNull()
    java.lang.String description, @org.jetbrains.annotations.NotNull()
    bertrand.maxence.unsplashTP.models.PhotoUrl url, @org.jetbrains.annotations.NotNull()
    bertrand.maxence.unsplashTP.models.User user, @org.jetbrains.annotations.NotNull()
    java.lang.String likes, @org.jetbrains.annotations.NotNull()
    bertrand.maxence.unsplashTP.models.Sponsorship sponsorship, @org.jetbrains.annotations.NotNull()
    bertrand.maxence.unsplashTP.models.Exif exif) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public Photo(@org.jetbrains.annotations.NotNull()
    java.lang.String id, @org.jetbrains.annotations.NotNull()
    java.lang.String width, @org.jetbrains.annotations.NotNull()
    java.lang.String height, @org.jetbrains.annotations.NotNull()
    java.lang.String color, @org.jetbrains.annotations.NotNull()
    java.lang.String description, @org.jetbrains.annotations.NotNull()
    bertrand.maxence.unsplashTP.models.PhotoUrl url, @org.jetbrains.annotations.NotNull()
    bertrand.maxence.unsplashTP.models.User user, @org.jetbrains.annotations.NotNull()
    java.lang.String likes, @org.jetbrains.annotations.NotNull()
    bertrand.maxence.unsplashTP.models.Sponsorship sponsorship, @org.jetbrains.annotations.NotNull()
    bertrand.maxence.unsplashTP.models.Exif exif) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getId() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getWidth() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getHeight() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component4() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getColor() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component5() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getDescription() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final bertrand.maxence.unsplashTP.models.PhotoUrl component6() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final bertrand.maxence.unsplashTP.models.PhotoUrl getUrl() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final bertrand.maxence.unsplashTP.models.User component7() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final bertrand.maxence.unsplashTP.models.User getUser() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component8() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getLikes() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final bertrand.maxence.unsplashTP.models.Sponsorship component9() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final bertrand.maxence.unsplashTP.models.Sponsorship getSponsorship() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final bertrand.maxence.unsplashTP.models.Exif component10() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final bertrand.maxence.unsplashTP.models.Exif getExif() {
        return null;
    }
}